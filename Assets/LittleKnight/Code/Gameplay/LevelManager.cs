﻿using UnityEngine;

public class LevelManager : Singleton<LevelManager>
{

    [SerializeField]
    ColorPalette palette;

    public Camera MainCamera;
    public SpriteRenderer PrimaryRend;
    public SpriteRenderer[] SecondaryRend;

    public LittleKnight.Bounds LevelBounds;

    public Level CurrentLevel;

    private void Start()
    {
        CurrentLevel = FindObjectOfType<Level>();
    }
}
