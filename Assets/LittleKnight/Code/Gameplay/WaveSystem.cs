﻿
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;

public class WaveSystem : MonoBehaviour
{

    public GameObject ItemPrefab;
    public GameObject EnemyPrefab;

    public Transform ItemContainer;
    public Transform EnemyContainer;

    //List<SpawnPoint> used = new List<SpawnPointInfo>();
    List<SpawnPoint> available;

    public bool PointsLoaded { get; private set; }
    public bool ItemsSpawned { get; private set; }
    public bool EnemySpawned { get; private set; }

    public List<SpawnPoint> spawnPoints = new List<SpawnPoint>();

    public int Count;

    private void Awake()
    {
        PointsLoaded = false;
        ItemsSpawned = false;
        EnemySpawned = false;
    }

    private void OnEnable()
    {
        GameManager.OnSpawnPointsLoaded += OnSpawnPointsLoaded;
    }
    private void OnDisable()
    {
        GameManager.OnSpawnPointsLoaded -= OnSpawnPointsLoaded;
    }

    private void OnSpawnPointsLoaded(SpawnPoints points)
    {
        //spawnPoints = points;
        //PointsLoaded = true;
    }

    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.I))
        {
            SpawnItems(Count);
        }

        if (Input.GetKeyDown(KeyCode.E))
        {
            SpawnEnemy(Count);
        }

        if (Input.GetKeyDown(KeyCode.S))
        {
            SpawnWave(5);
        }
    }

    public void SpawnItems(int count)
    {
        if (PointsLoaded)
        {
            available = new List<SpawnPoint>();
            for (int i = 0; i < spawnPoints.Count; i++)
            {
                available.Add(spawnPoints[i]);
            }
            //used = new List<SpawnPointInfo>();

            if (count > available.Count)
            {
                return;
            }

            for (int i = 0; i < count; i++)
            {
                int random = Random.Range(0, available.Count);
                Transform t = Instantiate(ItemPrefab, ItemContainer).transform;
                t.position = available[random].info.Position;
                available.RemoveAt(random);
                ItemsSpawned = true;
            }
        }
       
    }

    public void SpawnEnemy(int count)
    {
        //if (PointsLoaded)
        //{
        //    available = spawnPoints.Points.ToList();
        //    used = new List<SpawnPointInfo>();

        //    if (count > available.Count)
        //    {
        //        return;
        //    }

        //    for (int i = 0; i < count; i++)
        //    {
        //        Transform t = Instantiate(EnemyPrefab, EnemyContainer).transform;
        //        Enemy e = t.GetComponentInChildren<Enemy>();
        //        int random = Random.Range(0, available.Count);
        //        e.transform.position = available[random].Position;
        //        e.PointA = available[random].PointA;
        //        e.PointB = available[random].PointB;
        //        available.RemoveAt(random);
        //        EnemySpawned = true;
        //    }
        //}
    }

    public void SpawnWave(int number)
    {
        
        available = new List<SpawnPoint>();
        for (int i = 0; i < spawnPoints.Count; i++)
        {
            available.Add(spawnPoints[i]);
        }

        if (number + 1 > available.Count)
        {
            number = available.Count - 1;
        }
        int rand = Random.Range(0, available.Count);
        //GameManager.Instance.player.Init(available[rand].info.Position);
        available.RemoveAt(rand);
        for (int i = 0; i < number; i++)
        {
            Transform enemy = Instantiate(EnemyPrefab, EnemyContainer).transform;
            Enemy e = enemy.GetComponentInChildren<Enemy>();
            int random = Random.Range(0, available.Count);
            e.transform.position = available[random].info.PointA;
            e.PointA = available[random].info.PointA;
            e.PointB = available[random].info.PointB;
            Transform item = Instantiate(ItemPrefab, ItemContainer).transform;
            GameManager.Instance.itemsRemaining++;
            item.position = available[random].info.Position;
            available.RemoveAt(random);
        }
    }
}
