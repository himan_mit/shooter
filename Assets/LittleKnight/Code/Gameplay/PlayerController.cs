﻿using Pathfinding;
using UnityEngine;
using System.Collections;

[RequireComponent(typeof(Rigidbody2D))]
[RequireComponent(typeof(Seeker))]
public class PlayerController : Shooter
{
    public Transform target;
    //private Seeker seeker;
    private Rigidbody2D rb;
    
    //[HideInInspector]
    //public bool pathIsEnded = false;

    //public float nextWaypointDistance = 3f;
    //private int currentWyPoint = 0;

    public Camera Cam;

    float yOffset;
    float xOffset;

    SpriteRenderer rend;

    public float cameradistance = 5;
    public float cameraSpeed = 2;
    private void Awake()
    {
        rend = GetComponent<SpriteRenderer>();
    }

    private void Start()
    {
        id = ++Counter;
        rb = GetComponent<Rigidbody2D>();
        //StartCoroutine(UpdatePath());

        yOffset = (float)Screen.height / (float)Screen.width;
        xOffset = (float)Screen.width / (float)Screen.height;

    }

    public override void TakeDamage(int damage)
    {
        base.TakeDamage(damage);
        float value = (100f - Health) / 100f;
        rend.color = new Color(value, value, value);
        if (Health <= 0)
            return;
        switch(Random.Range(0, 5))
        {
            case 0:
                AudioManager.Instance.PlayAnnouncer("war_medic");
                break;
            case 1:
                AudioManager.Instance.PlayAnnouncer("war_call_for_backup");
                break;
            case 2:
                AudioManager.Instance.PlayAnnouncer("war_get_down");
                break;
            case 3:
                AudioManager.Instance.PlayAnnouncer("war_look_out");
                break;
            default:
                break;
        }
    }

    private void OnEnable()
    {
        GameManager.OnPaletteChange += OnPaletteChange;
    }

    private void OnDisable()
    {
        GameManager.OnPaletteChange += OnPaletteChange;
    }

    private void OnPaletteChange(ColorPalette palette)
    {
        if (palette != null)
        {
            rend.material.color = palette.Extra;
        }
    }

    public void Heal(int v)
    {
        Health += v;
        if (Health > 100)
            Health = 100;
        float value = (100f - Health) / 100f;
        rend.color = new Color(value, value, value);
    }

    //private void UpdatePath()
    //{
    //while (true)
    //{
    //if (target != null)
    //{
    //    seeker.StartPath(transform.position, target.position, OnPathComplete);
    //}
    //    yield return new WaitForSeconds(1f / updateRate);
    //}
    //}

    private void Update()
    {
        if (Time.timeScale < 1)
            return;
        if (Input.GetMouseButtonUp(1))
        {
            MoveTarget(Input.mousePosition);
        }
        if (Input.GetMouseButtonUp(0))
        {
            Vector3 targetPosition = Cam.ScreenToWorldPoint(Input.mousePosition);
            targetPosition.z = 0;
            Shoot(targetPosition);
        }
        if (Vector3.Distance(transform.position, Cam.transform.position) > cameradistance)
            Cam.transform.position = Vector3.Lerp(Cam.transform.position, LevelManager.Instance.LevelBounds.Clamp(transform.position, Cam.transform.position.z, Cam.orthographicSize, xOffset, yOffset), Time.deltaTime * cameraSpeed);
    }

    public override void Shoot(Vector3 mousePosition)
    {
        base.Shoot(mousePosition);
        AudioManager.Instance.PlayGun();
    }

    private void MoveTarget(Vector3 mousePosition)
    {
        Vector3 newPosition = Cam.ScreenToWorldPoint(mousePosition);
        newPosition.z = 0;
        target.position = newPosition;
        //UpdatePath();
    }

    //private void OnPathComplete(Path p)
    //{
    //    Debug.Log("Found Path. Error?" + p.error);
    //    if(!p.error)
    //    {
    //        path = p;
    //        currentWyPoint = 0;
    //    }
    //}

    public override void Die()
    {
        ParticleSystem effect = Instantiate(DeathEffect, transform.position, transform.rotation);
        Destroy(effect, DeathEffectDuration);
        rend.enabled = false;
        GameManager.Instance.GameOver();
    }

    public void Init(Vector3 position)
    {
        transform.position = position;
        //target.position = position;
    }

    private void OnCollisionEnter2D(Collision2D other)
    {
        if(other.collider.tag == "Item")
        {
            GameManager.Instance.itemsRemaining--;
            Destroy(other.collider.gameObject);
            GameManager.Instance.Score += 10;
            Heal(5);
            if(GameManager.Instance.itemsRemaining > 0)
                AudioManager.Instance.PlayAnnouncer("power_up");
            GameManager.Instance.CheckLevelComplete();
            Debug.Log("Item Collected");
        }
    }
    
}
