﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpawnPoint : MonoBehaviour
{
    public SpawnPointInfo info;
}

[System.Serializable]
public class SpawnPointInfo
{
    public Vector3 Position;
    public Vector3 PointA;
    public Vector3 PointB;
}