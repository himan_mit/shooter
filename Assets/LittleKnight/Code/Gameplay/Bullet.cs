﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bullet : MonoBehaviour
{
    public float speed;
    int damage;
    public ParticleSystem blastParticles;
    int parentID;

    Vector3 direction;

    private void Start()
    {
        Destroy(gameObject, 3);
    }

    public void Shoot(Vector3 TargetPosition, int parentID, int damage)
    {
        this.parentID = parentID;
        this.damage = damage;
        direction = (TargetPosition - transform.position).normalized;
    }

    private void Update()
    {
        float distanceThisFrame = speed * Time.deltaTime;
        transform.Translate(direction * distanceThisFrame, Space.World);
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision != null)
        {
            if (collision.tag != "Wall")
            {
                Shooter shooter = collision.GetComponent<Shooter>();
                if (shooter != null && shooter.id != parentID)
                {
                    shooter.TakeDamage(damage);
                    DestroyBullet();
                }
            }
        }
    }

    private void DestroyBullet()
    {
        ParticleSystem effect = Instantiate(blastParticles, transform.position, Quaternion.FromToRotation(transform.position, -direction));
        Destroy(effect, 0.5f);
        Destroy(gameObject);
    }
}
