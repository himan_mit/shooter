﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;
using UnityEngine.UI;
using System;
using DG.Tweening;
using UnityEngine.SceneManagement;

public class GameManager : Singleton<GameManager>
{
    public PlayerController player;

    Dictionary<string, ColorPalette> palettes = new Dictionary<string, ColorPalette>();

    private ColorPalette palette;

    [SerializeField]
    SpawnPoints spawnPoints;

    public delegate void PaletteAction(ColorPalette palette);
    public static event PaletteAction OnPaletteChange;

    public delegate void SpawnPointsAction(SpawnPoints points);
    public static event SpawnPointsAction OnSpawnPointsLoaded;

    int score = 0;
    public int Score
    {
        get { return score; }
        set
        {
            score = value;
            UpdateScore();
        }
    }


    int HighScore = 0;

    public WaveSystem waveSystem;

    public int itemsRemaining = 0;
    bool isHiScore = false;

    private void Start()
    {
        if (PlayerPrefs.HasKey("HighScore"))
            HighScore = PlayerPrefs.GetInt("HighScore");
        UpdateScore();
        LoadPalettes();
        LoadSpawnPoints();
        //if(PlayerPrefs.HasKey("Sound"))

        if (palettes.Count > 0)
        {
            //SetPalette(palettes.First().Value.ID);
        }
        if (Time.time < 1)
            ToggleMenu(true);
        else
            GameStart();
    }


    public void LoadPalettes()
    {
        ColorPalette[] loaded = Resources.LoadAll<ColorPalette>("Palettes/");
        foreach (ColorPalette p in loaded)
        {
            palettes.Add(p.ID, p);
        }
    }

    public void LoadSpawnPoints()
    {
        try
        {
            string text = Resources.Load<TextAsset>("SpawnPoints").ToString();
            spawnPoints = JsonUtility.FromJson<SpawnPoints>(text);
            if (OnSpawnPointsLoaded != null)
            {
                OnSpawnPointsLoaded(spawnPoints);
            }
        }
        catch (System.Exception)
        {
            Debug.Log("Unable to load spawn points!");
        }

    }

    public void SetPalette(string id)
    {
        if (palettes.ContainsKey(id))
        {
            palette = palettes[id];
            if (OnPaletteChange != null)
            {
                OnPaletteChange(palette);
            }
        }
    }

    public SpawnPoints GetSpawnPoints()
    {
        return spawnPoints;
    }

    public void GameOver()
    {
        ToggleGameOverPanel(true);
        AudioManager.Instance.PlayAnnouncer("game_over");
        Invoke("CheckHighScore", 2 * Time.timeScale);
    }

    void CheckHighScore()
    {
        if (isHiScore)
            AudioManager.Instance.PlayAnnouncer("new_highscore");
    }

    public void GameStart()
    {
        Time.timeScale = 1;
        ToggleMenu(false);
        ToggleGamePanel(true);
        StartCoroutine(SpawnWave());
    }

    public IEnumerator SpawnWave()
    {
        yield return new WaitForSeconds(2);
        int timeRemaining = 3;
        SetCounterText("Ready");
        AudioManager.Instance.PlayAnnouncer("ready");
        yield return new WaitForSeconds(1);
        while (timeRemaining > 0)
        {
            SetCounterText(timeRemaining.ToString());
            AudioManager.Instance.PlayAnnouncer(timeRemaining.ToString());
            yield return new WaitForSeconds(1);
            timeRemaining--;
        }
        CounterText.text = "Go";
        AudioManager.Instance.PlayAnnouncer("Go");
        yield return new WaitForSeconds(0.5f);
        CounterText.gameObject.SetActive(false);
        waveSystem.SpawnWave(5);
    }

    public void CheckLevelComplete()
    {
        if (itemsRemaining == 0)
        {
            AudioManager.Instance.PlayAnnouncer("objective_achieved");
            GameStart();
        }
    }


    public void Restart()
    {
        SceneManager.LoadScene(0);
    }

    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.R))
        {
            Restart();
        }
        if (Input.GetKeyDown(KeyCode.Delete))
            PlayerPrefs.DeleteAll();
        if (Input.GetKeyDown(KeyCode.Escape))
            ToggleMenu(true);
    }

    public void Quit()
    {
        Application.Quit();
    }

    #region UI
    [Header("UI")]
    public Text[] ScoreText;
    public Text[] HighScoreText;
    public Text CounterText;
    public GameObject MenuPanel;
    public GameObject GameOverPanel;
    public GameObject GamePanel;

    public void ToggleGameOverPanel(bool value)
    {
        if (value)
        {
            GameOverPanel.gameObject.SetActive(true);
            GamePanel.gameObject.SetActive(false);
            MenuPanel.gameObject.SetActive(false);
            Time.timeScale = 0.3f;
        }
        else
        {
            GameOverPanel.gameObject.SetActive(false);
            Time.timeScale = 1;
        }
    }

    public void SetCounterText(string value)
    {
        CounterText.gameObject.SetActive(true);
        CounterText.text = value;
        CounterText.DOFade(0, 0);
        CounterText.DOFade(1, 0.7f);
        CounterText.transform.DOScale(5, 0);
        CounterText.transform.DOScale(1, 0.7f);
    }

    public void UpdateScore()
    {
        foreach (Text text in ScoreText)
        {
            text.text = "Score:" + Score.ToString();
        }
        if (Score > HighScore)
        {
            isHiScore = true;
            HighScore = Score;
        }
        foreach (Text text in HighScoreText)
        {
            text.text = "HiScore:" + HighScore.ToString();
        }
        PlayerPrefs.SetInt("HighScore", HighScore);
    }

    public void ToggleMenu(bool value)
    {
        if (value)
        {
            MenuPanel.gameObject.SetActive(true);
            GamePanel.gameObject.SetActive(false);
            GameOverPanel.gameObject.SetActive(false);
            Time.timeScale = 0;
        }
        else
        {
            MenuPanel.gameObject.SetActive(false);
            Time.timeScale = 1;
        }
    }

    public void ToggleGamePanel(bool value)
    {
        if (value)
        {
            MenuPanel.gameObject.SetActive(false);
            GamePanel.gameObject.SetActive(true);
            GameOverPanel.gameObject.SetActive(false);
            Time.timeScale = 1;
        }
        else
        {
            GamePanel.gameObject.SetActive(false);
            //Time.timeScale = 1;
        }
    }

    //public void SetSoundIcon(bool value)
    //{

    //}
    #endregion
}
