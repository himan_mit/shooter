﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class Shooter : MonoBehaviour
{
    public static int Counter = 0;

    public int Health;
    public int Damage;
    public Bullet BulletPrefab;

    public int id;

    public ParticleSystem DeathEffect;
    public float DeathEffectDuration = 1;
    

    public virtual void TakeDamage(int damage)
    {
        Health -= damage;
        if (Health <= 0)
            Die();
    }

    public virtual void Die()
    {
        ParticleSystem effect = Instantiate(DeathEffect, transform.position, transform.rotation);
        Destroy(effect, DeathEffectDuration);
        Destroy(gameObject);
    }

    public virtual void Shoot(Vector3 targetPosition)
    {
        Bullet bullet = Instantiate(BulletPrefab, transform.position, Quaternion.identity);
        Vector3 shootDir = targetPosition;
        bullet.Shoot(shootDir, id, Damage);
    }
}
