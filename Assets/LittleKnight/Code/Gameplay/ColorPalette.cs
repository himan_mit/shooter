﻿using System;
using UnityEngine;

public class ColorPalette : ScriptableObject
{ 
    public string ID;

    public Color Base;
    public Color Primary;
    public Color Secondary;
    public Color Extra;

}
