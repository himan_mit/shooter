﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;
using System;

public class Level : MonoBehaviour
{

    public SpriteRenderer BackgroundRend;
    //public SpriteRenderer PrimaryRend;
    public Transform Walls;
    public Transform Boundaries;

    public Vector2 MinBounds;
    public Vector2 MaxBounds;

    [SerializeField]
    LittleKnight.Bounds Bounds;

    List<SpriteRenderer> wallsRend = new List<SpriteRenderer>();

    List<Vector3> boundaries = new List<Vector3>();

    // Use this for initialization
    void Start()
    {
        wallsRend.AddRange(Walls.GetComponentsInChildren<SpriteRenderer>().ToList());
        wallsRend.AddRange(Boundaries.GetComponentsInChildren<SpriteRenderer>().ToList());
        LoadBounds();
    }

    private void OnEnable()
    {
        GameManager.OnPaletteChange += OnPaletteChange;
    }

    private void OnDisable()
    {
        GameManager.OnPaletteChange -= OnPaletteChange;
    }

    private void OnPaletteChange(ColorPalette palette)
    {
        ////BackgroundRend.color = Color.white;
        BackgroundRend.material.color = palette.Secondary;
        foreach (var v in wallsRend)
        {
            v.color = Color.white;
            v.material.color = palette.Base;
        }
    }

    void LoadBounds()
    {
        boundaries.Clear();
        Transform[] transforms = Boundaries.GetComponentsInChildren<Transform>();
        foreach (Transform t in transforms)
        {
            boundaries.Add(t.position);
        }

        Bounds = new LittleKnight.Bounds();
        Bounds.Min = new Vector2(boundaries.Find(p => p.x < 0f).x, boundaries.Find(p => p.y < 0f).y);
        Bounds.Max = new Vector2(boundaries.Find(p => p.x > 0f).x, boundaries.Find(p => p.y > 0f).y);

        LevelManager.Instance.LevelBounds = Bounds;

    }

}
