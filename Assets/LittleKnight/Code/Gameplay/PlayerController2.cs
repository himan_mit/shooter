﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController2 : MonoBehaviour
{

    public Camera MainCam;
    float yOffset;
    float xOffset;

    float speed = 6f;

    void Start()
    {
        yOffset = (float)Screen.height / (float)Screen.width;
        xOffset = (float)Screen.width / (float)Screen.height;
    }

    // Update is called once per frame
    void Update ()
    {
        if (Input.GetKey(KeyCode.W))
        {
            transform.position += Vector3.up * Time.deltaTime * speed;
        }
        if (Input.GetKey(KeyCode.S))
        {
            transform.position += Vector3.down * Time.deltaTime * speed;
        }

        if (Input.GetKey(KeyCode.A))
        {
            transform.position += Vector3.left * Time.deltaTime * speed;
        }
        if (Input.GetKey(KeyCode.D))
        {
            transform.position += Vector3.right * Time.deltaTime * speed;
        }
        MainCam.transform.position = LevelManager.Instance.LevelBounds.Clamp(transform.position, MainCam.transform.position.z, MainCam.orthographicSize, xOffset, yOffset);
    }
}
