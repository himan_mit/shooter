﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AudioManager : Singleton<AudioManager> {

    public AudioSource Announcer;
    public AudioSource Gun;
    public AudioSource BG;

    public List<AudioClip> clips;
    
    //public void ToggleSound(bool value)
    //{
    //    if (value)
    //    {
    //        Announcer.volume = 1;
    //        Gun.volume = 1;
    //        BG.volume = 1;
    //        PlayerPrefs.SetInt("Sound", 1);
    //        GameManager.Instance.SetSoundIcon(true);
    //    }
    //    else
    //    {
    //        Announcer.volume = 0;
    //        Gun.volume = 0;
    //        BG.volume = 0;
    //        PlayerPrefs.SetInt("Sound", 0);
    //        GameManager.Instance.SetSoundIcon(false);
    //    }
    //}

    public void PlayAnnouncer(string clip)
    {
        if (Announcer.isPlaying && clip != "game_over")
            return;
        AudioClip audioClip = clips.Find(c => c.name == clip);
        Announcer.clip = audioClip;
        Announcer.Play();
    }

    public void PlayGun(string clip = "")
    {
        if (clip != "")
        {
            AudioClip audioClip = clips.Find(c => c.name == clip);
            Gun.clip = audioClip;
        }
        Gun.pitch = Random.Range(1, 1.5f);
        Gun.Play();
    }

    public void PlayBG(string clip)
    {
        AudioClip audioClip = clips.Find(c => c.name == clip);
        BG.clip = audioClip;
        BG.Play();
    }

}
