﻿using System;
using Pathfinding;
using UnityEngine;

[RequireComponent(typeof(Seeker))]
[RequireComponent(typeof(AILerp))]
public class Enemy : Shooter
{
    public Vector3 PointA;
    public Vector3 PointB;
    public float AggroDistance;
    public float ChangeDistance;

    public Transform Target;

    private Seeker seeker;
    private bool seekingPlayer = false;

    public float ShootRate;
    float timeToShoot = 0.5f;

    public int HealAmount = 5;
    private void Start()
    {
        id = ++Counter;
        seeker = GetComponent<Seeker>();
        PointA = transform.position;
        Target.position = PointA;
    }

    private void Update()
    {
        if (seekingPlayer)
        {
            Target.position = GameManager.Instance.player.transform.position;

            if (Time.time > timeToShoot)
            {
                Vector3 direction = (Target.position - transform.position).normalized;
                RaycastHit2D[] lineOfSight = Physics2D.RaycastAll(transform.position, direction, AggroDistance);
                if (lineOfSight.Length > 1)
                {
                    if (lineOfSight[1].collider.tag == "Player")
                    {
                        timeToShoot = Time.time + 1 / ShootRate;
                        Shoot(Target.position);
                    }
                }
            }
            return;
        }
        float distanceToPlayer = Vector3.Distance(transform.position, GameManager.Instance.player.transform.position);
        if (distanceToPlayer <= AggroDistance && !seekingPlayer)
        {
            seekingPlayer = true;
            return;
        }

        float distanceToA = Vector3.Distance(transform.position, PointA);
        float distanceToB = Vector3.Distance(transform.position, PointB);

        if (distanceToA <= ChangeDistance)
            Target.position = PointB;
        else if (distanceToB <= ChangeDistance)
            Target.position = PointA;
    }

    private void Patrol()
    {
        //patrolling = true;

    }

    public override void TakeDamage(int damage)
    {
        base.TakeDamage(damage);
    }

    public override void Die()
    {
        base.Die();
        AudioManager.Instance.PlayAnnouncer("war_target_destroyed");
        GameManager.Instance.player.Heal(HealAmount);
    }

    public override void Shoot(Vector3 mousePosition)
    {
        base.Shoot(mousePosition);
    }
}
