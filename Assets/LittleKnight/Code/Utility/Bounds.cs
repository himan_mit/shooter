﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace LittleKnight
{
    [System.Serializable]
    public class Bounds
    {
        public Vector2 Min;
        public Vector2 Max;

        public void SetBounds(Vector2 min, Vector2 max)
        {
            Min = min;
            Max = max;
        }

        public bool IsWithin(Vector3 position)
        {
            return position.x >= Min.x && position.x <= Max.x && position.y >= Min.y && position.y <= Max.y;
        }

        public Vector3 Clamp(Vector3 position)
        {
            Vector3 v = new Vector3(Mathf.Clamp(position.x, Min.x, Max.x), Mathf.Clamp(position.y, Min.y, Max.y), position.z);
            return v;
        }

        public Vector3 Clamp(Vector3 position, float z, float size)
        {
            Vector3 v = new Vector3(Mathf.Clamp(position.x, Min.x + size, Max.x - size), Mathf.Clamp(position.y, Min.y + size, Max.y - size), z);
            return v;
        }

        public Vector3 Clamp(Vector3 position, float z, float size, float xOffset, float yOffset)
        {
            Vector3 v = new Vector3(Mathf.Clamp(position.x, Min.x + size, Max.x - size), Mathf.Clamp(position.y, Min.y + (size * yOffset), Max.y - (size * yOffset)), z);
            return v;
        }

    }
}