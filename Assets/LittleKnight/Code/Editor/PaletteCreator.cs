﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

public class PaletteCreatorWindow : EditorWindow
{

    string id;
    Color background;
    Color primary;
    Color secondary;
    Color extra;

    [MenuItem("Little Knight/Art/Create Palette")]
    static void OpenWindow()
    {
        PaletteCreatorWindow window = (PaletteCreatorWindow)GetWindow(typeof(PaletteCreatorWindow));
        window.minSize = new Vector2(240, 160);
        window.maxSize = new Vector2(240, 160);

        window.titleContent.text = "Palette Creator";
    }

    private void OnGUI()
    {
        EditorGUILayout.Space();
        id = EditorGUILayout.TextField("Palette ID", id);
        EditorGUILayout.Space();
        background = EditorGUILayout.ColorField("Base Color", background);
        primary = EditorGUILayout.ColorField("Primary Color", primary);
        secondary = EditorGUILayout.ColorField("Secondary Color", secondary);
        extra = EditorGUILayout.ColorField("Extra Color", extra);

        if (GUILayout.Button("Create"))
        {
            Create();
        }

    }

    void Create()
    {
        ColorPalette cp = CreateInstance<ColorPalette>();
        cp.Base = background;
        cp.Primary = primary;
        cp.Secondary = secondary;
        cp.Extra = extra;
        cp.ID = id;
        AssetDatabase.CreateAsset(cp, "Assets/Resources/Palettes/" + id + ".asset" );
        AssetDatabase.SaveAssets();
    }

}
