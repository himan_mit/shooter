﻿using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using System.IO;

public class SpawnPointCreator : EditorWindow
{

    [MenuItem("Little Knight/Create Spawn Points")]
    static void CreateSpawnPoints()
    {
        if (Selection.activeTransform != null)
        {
            SpawnPoint[] transforms = Selection.activeTransform.GetComponentsInChildren<SpawnPoint>();
            if (transforms.Length > 0)
            {
                SpawnPoints spawnPoints = new SpawnPoints();
                List<SpawnPointInfo> points = new List<SpawnPointInfo>();
                foreach (SpawnPoint t in transforms)
                {
                    points.Add(t.GetComponent<SpawnPoint>().info);
                }
                spawnPoints.Points = points.ToArray();
                WriteToFile(spawnPoints);
            }
            else
            {
                Debug.Log("SPAWN POINTS: Selected gameobject does not have any children!");
            }
        }
        else
        {
            Debug.Log("SPAWN POINTS: Select a gameobject first!");
        }
    }

    static void WriteToFile(SpawnPoints points)
    {
        string path = null;

        path = "Assets/Resources/SpawnPoints.json";

        string str = JsonUtility.ToJson(points);
        using (FileStream fs = new FileStream(path, FileMode.Create))
        {
            using (StreamWriter writer = new StreamWriter(fs))
            {
                writer.Write(str);
            }
        }
        UnityEditor.AssetDatabase.Refresh();
        Debug.Log("SPAWN POINTS: SAVED!");
    }
}